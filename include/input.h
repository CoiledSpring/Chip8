/* input.h --- 
 * 
 * Filename: input.h
 * Author: Jules <archjules>
 * Created: Sat Dec  3 01:20:40 2016 (+0100)
 * Last-Updated: Sat Dec  3 12:09:26 2016 (+0100)
 *           By: Jules <archjules>
 */

#ifndef INPUT_H
#define INPUT_H
#include <SDL/SDL.h>
#include <stdint.h>
#include "cpu.h"

void treat_events(struct CPU * cpu);
int determine_key(uint8_t key);
int wait_for(struct CPU * cpu);

#endif /* INPUT_H */
