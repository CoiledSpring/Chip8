/* screen.h --- 
 * 
 * Filename: screen.h
 * Author: Jules <archjules>
 * Created: Fri Dec  2 21:14:41 2016 (+0100)
 * Last-Updated: Sat Dec  3 23:51:59 2016 (+0100)
 *           By: Jules <archjules>
 */
#include <SDL/SDL.h>

#ifndef SCREEN_H
#define SCREEN_H
#define SCREEN_MEMORY_SIZE 2048
#define SCREEN_WIDTH  64
#define SCREEN_HEIGHT 32

struct Screen {
    char content[SCREEN_MEMORY_SIZE];
    SDL_Surface * window;
    int state; // 0 = The same
};

#include "cpu.h"
void screen_init(struct Screen * screen);
void screen_clear(struct Screen * screen);
void screen_draw8(struct Screen * screen, struct CPU * cpu, int x, int y, char * buf, int n);
void screen_flip(struct Screen * screen);
#endif /* SCREEN_H */
