/* cpu.h --- 
 * 
 * Filename: cpu.h
 * Author: Jules <archjules>
 * Created: Fri Dec  2 16:44:04 2016 (+0100)
 * Last-Updated: Sat Dec  3 17:15:52 2016 (+0100)
 *           By: Jules <archjules>
 */
#ifndef CPU_H
#define CPU_H
#include <stdint.h>

#define MEMORY_SIZE 0x1000

struct CPU {
    // Memory
    uint8_t memory[MEMORY_SIZE];

    // Registers
    uint16_t PC;
    uint8_t  registers[16];
    uint16_t addr_register;

    // Stack
    uint16_t stack[16];
    uint16_t SP;

    // Timers
    uint8_t  delay_timer;
    uint8_t  sound_timer;

    // Input
    uint8_t  keys[16];

    // Misc
    int state; // 0 = ON, 1 = OFF
};

#include "screen.h"
void cpu_init(struct CPU * cpu);
void cpu_load_file(struct CPU * cpu, char * filename);
void execute_next_instruction(struct CPU * cpu, struct Screen * screen);
void execute_master_0(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_1(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_2(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_3(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_4(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_5(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_6(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_7(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_8(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_9(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_A(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_B(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_C(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_D(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_E(struct CPU * cpu, struct Screen * screen, uint16_t instruction);
void execute_master_F(struct CPU * cpu, struct Screen * screen, uint16_t instruction);

#endif /* CPU_H */
