# Yet another Chip8 Interpreter.
## Compilation

To compile it, you just have to run :
    
    cmake .
    make
    
## Usage

To run it, call the `chip8` executable with the filename of the ROM as an argument.