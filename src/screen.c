/* screen.c --- 
 * 
 * Filename: screen.c
 * Author: Jules <archjules>
 * Created: Fri Dec  2 21:18:47 2016 (+0100)
 * Last-Updated: Sat Dec  3 23:51:46 2016 (+0100)
 *           By: Jules <archjules>
 */
#include <stdlib.h>
#include "logger.h"
#include "cpu.h"
#include "screen.h"

/*
 * screen_init:
 * Initialise a Screen struct
 * Side effect: Creates a SDL Window, and fills it with black.
 */
void screen_init(struct Screen * screen) {
    bzero(screen->content, SCREEN_MEMORY_SIZE);

    screen->window = SDL_SetVideoMode(512, 256, 8, SDL_HWSURFACE | SDL_DOUBLEBUF);
    if (screen->window == NULL) {
	log_fatal("Couldn't set video mode.");
	exit(EXIT_FAILURE);
    }
    log_info("Video Mode set.");

    SDL_WM_SetCaption("Chip8 Interpreter", NULL);
    SDL_FillRect(screen->window, NULL, 0);
}

/*
 * screen_clear:
 * Clears the screen (fills it with black).
 */
void screen_clear(struct Screen * screen) {
    bzero(screen->content, SCREEN_MEMORY_SIZE);
    SDL_FillRect(screen->window, NULL, 0);
}

/* screen_draw8:
 * Draws a 8xn sprite at the emplacement (x,y) on the screen. If there is a collision, sets VF to 1.
 *
 * @arg buf A pointer to the sprite
 * @arg n   The height of the sprite
 */
void screen_draw8(struct Screen * screen, struct CPU * cpu, int x, int y, char * buf, int n) {
    for (int i = 0; i < n; i++) {
	for (int j = 0; j < 8; j++) {
	    if ((buf[i] & (0x80>>j)) && ((x + j) < SCREEN_WIDTH) && ((y + i) < SCREEN_HEIGHT)) {
		if (screen->content[64*(y + i) + (x + j)]) cpu->registers[0xF] = 1;
		screen->content[64*(y + i) + (x + j)] ^= 1;
	    }
	}
    }

    screen->state = 1;
}

/*
 * screen_flip:
 * Refreshes the actual screen, if there was any update since the last call.
 */
void screen_flip(struct Screen * screen) {
    static int last_updated = 0;
    SDL_Rect rect = {.w = 8, .h = 8};

    if (!screen->state) return;
    
    SDL_FillRect(screen->window, NULL, 0);
    for (int i = 0; i < SCREEN_MEMORY_SIZE; i++) {
	rect.x = (i % 64) * 8;
	rect.y = (i / 64) * 8;
	if (screen->content[i]) SDL_FillRect(screen->window, &rect, 0xFFFFFFFF);
    }

    screen->state = 0;
    SDL_Flip(screen->window);

    SDL_Delay(10);
}
