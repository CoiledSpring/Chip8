/* input.c --- 
 * Functions dealing with input (and events in general)
 * Filename: input.c
 * Author: Jules <archjules>
 * Created: Sat Dec  3 01:23:00 2016 (+0100)
 * Last-Updated: Sun Dec  4 00:18:46 2016 (+0100)
 *           By: Jules <archjules>
 */
#include <SDL/SDL.h>
#include "logger.h"
#include "input.h"

/*
 * treat_events:
 * Treats all events, if there is any.
 * Side effect: Updates cpu->keys.
 */
void treat_events(struct CPU * cpu) {
    SDL_Event event;
    int key;
    
    while(SDL_PollEvent(&event)) {
	switch(event.type) {
	case SDL_QUIT:
	    exit(EXIT_SUCCESS);
	case SDL_KEYDOWN:
	    key = determine_key(event.key.keysym.scancode);
	    if (key != -1) {
		cpu->keys[key] = 1;
		log_debug("Key %d pressed.", key);
	    }
	    break;
	case SDL_KEYUP:
	    key = determine_key(event.key.keysym.scancode);
	    if (key != -1) {
		cpu->keys[key] = 0;
		log_debug("Key %d released.", key);
	    }
	}
    }
}

/*
 * wait_for:
 * Waits for a keypress, and returns the key pressed.
 * Side effects: Updates cpu->keys if any keys are released.
 */
int wait_for(struct CPU * cpu) {
    int key;
    SDL_Event event;

    do {
	SDL_WaitEvent(&event);
	
	switch (event.type) {
	case SDL_QUIT:
	    exit(EXIT_SUCCESS);
	case SDL_KEYUP:
	    key = determine_key(event.key.keysym.scancode);
	    if (key != -1) {
		cpu->keys[key] = 0;
		log_debug("Key %d released.", key);
	    }
	}
    } while ((event.type != SDL_KEYDOWN) || (determine_key(event.key.keysym.scancode) == -1));

    return determine_key(event.key.keysym.scancode);
}

/*
 * determine_key:
 * Returns the key corresponding to the given scancode
 * 
 * @arg key The scancode of the key
 * @return  Number between 0 et 15, corresponding to the scancode,
 *          or -1 if the scancode doesn't correspond to any key of the keypad.
 */
int determine_key(uint8_t key) {
    switch(key) {
    case 90:  return 0x0;
    case 79:  return 0x1;
    case 80:  return 0x2;
    case 81:  return 0x3;
    case 83:  return 0x4;
    case 84:  return 0x5;
    case 85:  return 0x6;
    case 87:  return 0x7;
    case 88:  return 0x8;
    case 89:  return 0x9;
    case 114: return 0xA;
    case 91:  return 0xB;
    case 63:  return 0xC;
    case 82:  return 0xD;
    case 86:  return 0xE;
    case 104: return 0xF;
    }

    return -1;
}
