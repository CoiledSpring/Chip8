/* main.c --- 
 * 
 * Filename: main.c
 * Author: Jules <archjules>
 * Created: Fri Dec  2 16:29:08 2016 (+0100)
 * Last-Updated: Sat Dec  3 23:24:49 2016 (+0100)
 *           By: Jules <archjules>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "logger.h"
#include "screen.h"
#include "input.h"
#include "cpu.h"

/*
 * main:
 * Entry point of the program. Loads up the ROM, does some initialisation and enters in the main loop.
 * 
 * @arg argc The number of arguments
 * @arg argv The actual arguments
 * @return   0 in case of success.
 */
int main(int argc, char ** argv) {
    struct CPU cpu;
    struct Screen screen;
    char * rom_filename = NULL, c;
    int debug = 0, last_updated = 0;

    while((c = getopt(argc, argv, "dh")) != -1) {
	switch(c) {
	case 'h':
	    fprintf(stderr, "%s [-d] [-h] ROM_FILE\n", argv[0]);
	    return 0;
	case 'd':
	    debug = 1;
	}
    }

    if (optind == argc) {
        log_fatal("You must specify a rom file.");
	return 1;
    }
    rom_filename = argv[optind];

    srand(time(NULL));
    SDL_Init(SDL_INIT_VIDEO);
    
    cpu_init(&cpu);
    cpu_load_file(&cpu, rom_filename);
    screen_init(&screen);
    
    // Main loop
    while (1) {
	if (!cpu.state) execute_next_instruction(&cpu, &screen);

	treat_events(&cpu);

	// Timers
	if ((last_updated - SDL_GetTicks()) > 16) {
	    if (cpu.delay_timer != 0) cpu.delay_timer--;
	    if (cpu.sound_timer != 0) cpu.sound_timer--;
	    last_updated = SDL_GetTicks();
	}
	
	// Screen
	screen_flip(&screen);
    }
}
