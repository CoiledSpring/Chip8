/* cpu.c --- 
 * 
 * Filename: cpu.c
 * Author: Jules <archjules>
 * Created: Fri Dec  2 17:11:57 2016 (+0100)
 * Last-Updated: Sun Dec  4 01:22:39 2016 (+0100)
 *           By: Jules <archjules>
 */
#include <stdlib.h>
#include <endian.h>
#include <string.h>
#include "logger.h"
#include "screen.h"
#include "input.h"
#include "cpu.h"

unsigned char font8[] = {
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

void (*masters[])(struct CPU *, struct Screen *, uint16_t) = {execute_master_0, execute_master_1, execute_master_2, execute_master_3, execute_master_4, execute_master_5, execute_master_6, execute_master_7, execute_master_8, execute_master_9, execute_master_A, execute_master_B, execute_master_C, execute_master_D, execute_master_E, execute_master_F};

/*
 * cpu_init:
 * Initialise the CPU (fills the structure with zeroes, copies the font to memory, and sets PC to 0x200)
 */
void cpu_init(struct CPU * cpu) {
    bzero(cpu, sizeof(struct CPU));
    memcpy(cpu->memory, font8, sizeof(font8));
    cpu->PC = 0x200;
}

/*
 * cpu_load_file:
 * Opens the file, and copies it to the memory of the cpu.
 * 
 * @arg filename The filename of the file to open
 */
void cpu_load_file(struct CPU * cpu, char * filename) {
    FILE * file = fopen(filename, "rb");
  
    if (file == NULL) {
	log_fatal("Couldn't open the file %s.", filename);
	exit(EXIT_FAILURE);
    }

    fread(cpu->memory + 0x200, MEMORY_SIZE - 0x200, 1, file);
    fclose(file);
    log_info("Loaded the file %s.", filename);
}

/*
 * execute_next_instruction:
 * Executes the instruction at PC, and increments PC  by 2.
 */
void execute_next_instruction(struct CPU * cpu, struct Screen * screen) {
    uint16_t instruction = (cpu->memory[cpu->PC] << 8) + cpu->memory[cpu->PC + 1];

    masters[(instruction & 0xF000) >> 12](cpu, screen, instruction);
    
    cpu->PC+=2;
}

/*
 * execute_master_0:
 * This function deals with all opcodes beginning with 0x0,
 * namely CLEAR, RETURN, and issues a warning if the program tries to call a RCA1802 program.
 */
void execute_master_0(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    if (instruction == 0x00E0) {
	// CLEAR
	log_debug("CLEAR");
	screen_clear(screen);
    } else if (instruction == 0x00EE) {
	// RETURN
	log_debug("RETURN");
	cpu->SP--;
	cpu->PC = cpu->stack[cpu->SP];
    } else if (instruction == 0) {
	log_info("The CPU is now put to an halt");
	cpu->state = 1;
    } else {
	log_warn("Instruction not implemented : 0x%X", instruction);
    }
}

/*
 * execute_master_1:
 * Jumps to NNN.
 */
void execute_master_1(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    // GOTO
    int addr = (instruction & 0xFFF);
    log_debug("GOTO %#x", addr);
    cpu->PC = addr - 2;
}

/*
 * execute_master_2:
 * Puts the current PC on the stack, and jumps to NNN.
 */
void execute_master_2(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int addr = (instruction & 0xFFF);  
    log_debug("CALL %#x", addr);
    cpu->stack[cpu->SP] = cpu->PC;
    cpu->SP++;
    if (cpu->SP == 16) {
	log_fatal("Maximum call depth exceeded.");
	exit(EXIT_FAILURE);
    }
    cpu->PC = addr - 2;
}

/*
 * execute_master_3:
 * Skips the next instruction if VX == N
 */
void execute_master_3(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0xF00) >> 8, nn = (instruction & 0xFF);
    log_debug("JEQ V%X, %d", x, nn);
    if (cpu->registers[x] == nn) {
	cpu->PC += 2;
    }
}

/*
 * execute_master_4:
 * Skips the next instruction if VX != N
 */
void execute_master_4(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0xF00) >> 8, nn = (instruction & 0xFF);
    log_debug("JNE V%X, %d", x, nn);
    if (cpu->registers[(instruction & 0xF00) >> 8] != (instruction & 0xFF)) {
	cpu->PC += 2;
    }
}

/*
 * execute_master_5:
 * Skips the next instruction if VX == VY.
 */
void execute_master_5(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0xF00) >> 8, y = (instruction & 0xF0) >> 4;
    log_debug("JEQ V%X, V%X", x, y);
    if (cpu->registers[x] == cpu->registers[y]) {
	cpu->PC += 2;
    }
}

/*
 * execute_master_6:
 * Sets VX to NN.
 */
void execute_master_6(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0x0F00) >> 8, nn = (instruction & 0xFF);
    log_debug("MOV V%X, %d", x, nn);
    cpu->registers[x] = nn;
}

/*
 * execute_master_7:
 * Adds NN to VX.
 */
void execute_master_7(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0x0F00) >> 8, nn = (instruction & 0xFF);
    log_debug("ADD V%X, %d (%d)", x, nn, cpu->registers[x]);
    cpu->registers[x]+= nn;
}

/*
 * execute_master_8:
 * Deals with all opcodes beginning with 0x8, mainly arithmetic between registers.
 */
void execute_master_8(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0xF00) >> 8, y = (instruction & 0xF0) >> 4;
    switch (instruction & 0xF) {
    case 0x0:
	log_debug("MOV V%X, V%X", x, y);
	cpu->registers[x] = cpu->registers[y];
	break;
    case 0x1:
	log_debug("OR V%X, V%X", x, y);
	cpu->registers[x] |= cpu->registers[y];
	break;
    case 0x2:
	log_debug("AND V%X, V%X", x, y);
	cpu->registers[x] &= cpu->registers[y];
	break;
    case 0x3:
	log_debug("XOR V%X, V%X", x, y);
	cpu->registers[x] ^= cpu->registers[y];
	break;
    case 0x4:
	log_debug("ADD V%X, V%X", x, y);
	cpu->registers[x] += cpu->registers[y];
	break;
    case 0x5:
	log_debug("SUB V%X, V%X", x, y);
	cpu->registers[0xF] = (cpu->registers[x] > cpu->registers[y]);
	cpu->registers[x] -= cpu->registers[y];
	break;
    case 0x6:
	log_debug("SHR V%X", x);
	cpu->registers[0xF] = cpu->registers[x] & 1;
	cpu->registers[x] >>= 1;
	break;
    case 0x7:
	log_debug("OSB V%X, V%X", x, y);
	cpu->registers[0xF] = (cpu->registers[x] < cpu->registers[y]);
	cpu->registers[x] = cpu->registers[y] - cpu->registers[x];
	break;
    case 0xE:
	log_debug("SHL V%X", x);
	cpu->registers[0xF] = (cpu->registers[x] & 0x80) >> 7;
	cpu->registers[x] <<= 1;
	break;
    }
}

/*
 * execute_master_9:
 * Skips the next instruction if VX != VY.
 */
void execute_master_9(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0xF00) >> 8, y = (instruction & 0xF0) >> 4;
    log_debug("JNE V%X, V%X", x, y);
    if (cpu->registers[x] != cpu->registers[y]) {
	cpu->PC += 2;
    }
}

/*
 * execute_master_A:
 * Sets I to NNN.
 */
void execute_master_A(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    log_debug("MOV I, %d", instruction & 0xFFF);
    cpu->addr_register = (instruction & 0xFFF);
}

/*
 * execute_master_B:
 * Jumps to (V0 + NNN)
 */
void execute_master_B(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int nnn = (instruction & 0xFFF);
    log_debug("JMP V0+%d", nnn);
    cpu->PC = cpu->registers[0] + nnn - 2;
}

/*
 * execute_master_C:
 * Sets VX to rand()&N
 */
void execute_master_C(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0xF00) >> 8, n = (instruction & 0xFF);
    int r = (rand() % 0xFF) & n;
    
    log_debug("RAND V%X, %d = %d", x, n, r);
    cpu->registers[x] = r;
}

/*
 * execute_master_D:
 * Draws a 8xN sprite at (VX, VY). VF is set to 1 if there was any collision.
 */
void execute_master_D(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int vx = (instruction & 0xF00) >> 8, vy = (instruction & 0xF0) >> 4, n = (instruction & 0xF);
    int x  = cpu->registers[vx], y = cpu->registers[vy];
    
    log_debug("DRAW %d, %d, %d", x, y, n);
    screen_draw8(screen, cpu, x, y, cpu->memory + cpu->addr_register, n);
}

/*
 * execute_master_E:
 * Two opcodes dealing with input, the first skips the next instruction if the key in VX is pressed.
 * The second skips the next instruction if the key in VX isn't pressed.
 */
void execute_master_E(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0xF00) >> 8;
    
    if ((instruction & 0xFF) == 0x9E) {
	log_debug("JKP V%X (0x%x)", x, cpu->registers[x]);
	if (cpu->keys[cpu->registers[x]])  cpu->PC+=2;
    } else if ((instruction & 0xFF) == 0xA1) {
	log_debug("JKR V%X (0x%x)", x, cpu->registers[x]);
	if (!cpu->keys[cpu->registers[x]]) cpu->PC+=2;
    }
}

/*
 * execute_master_F:
 * This function deals with the opcodes beginning with 0xF.
 */
void execute_master_F(struct CPU * cpu, struct Screen * screen, uint16_t instruction) {
    int x = (instruction & 0xF00) >> 8;

    switch (instruction & 0xFF) {
    case 0x07:
	log_debug("GDT V%X", x);
	cpu->registers[x] = cpu->delay_timer;
	break;
    case 0x0A:
	log_debug("GKY V%X", x);
        cpu->registers[x] = wait_for(cpu);
	break;
    case 0x15:
	log_debug("SDT V%X", x);
	cpu->delay_timer = cpu->registers[x];
	break;
    case 0x18:
	log_debug("SST V%X", x);
	cpu->sound_timer = cpu->registers[x];
	break;
    case 0x1E:
	log_debug("ADI V%X", x);
	cpu->addr_register+=cpu->registers[x];
	break;
    case 0x29:
	// Sprite stuff
	log_debug("GSI V%X", x);
	cpu->addr_register = 5 * cpu->registers[x];
	break;
    case 0x33:
	// BCD
	log_debug("BCD V%X (%d)", x, cpu->registers[x]);
	cpu->memory[cpu->addr_register]     = cpu->registers[x] / 100;
	cpu->memory[cpu->addr_register + 1] = (cpu->registers[x] % 100) / 10;
	cpu->memory[cpu->addr_register + 2] = (cpu->registers[x] % 10);
	break;
    case 0x55:
	// Dump registers
	log_debug("DRG V%X", x);
	memcpy(cpu->memory + cpu->addr_register, cpu->registers, 1 + x);
	break;
    case 0x65:
	// Load registers
	log_debug("LRG V%X", x);
#if LOGLEVEL <= LOG_DEBG
	for (int i = 0; i <= x; i++) {
	    log_debug("  V%X: 0x%x", i,*(cpu->memory + cpu->addr_register + i));
	}
#endif
	memcpy(cpu->registers, cpu->memory + cpu->addr_register, 1 + x);
	break;
    }
}

